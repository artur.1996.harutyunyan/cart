<?php


namespace TravelShared\Test;


use PHPUnit\Framework\TestCase;
use TravelShared\Exceptions\DiscountAlreadyExists;
use TravelShared\Exceptions\DiscountDoesNotExistOnTheCart;
use TravelShared\Models\Cart;
use TravelShared\Models\Discount;
use TravelShared\Models\Product;
use TravelShared\Services\CartService;

class CartServiceTest extends TestCase
{
    /**
     * @return \PHPUnit\Framework\MockObject\MockObject
     */
    private function getCartMock()
    {
        return $this
            ->getMockBuilder(Cart::class)
            ->setMethods(
                [
                    'getProducts',
                    'getDiscount',
                    'addProduct',
                    'updateProductDiscount',
                    'removeProduct',
                    'addDiscount',
                    'removeDiscount',
                    'incrementProductQuantity'
                ]
            )
            ->getMock();
    }

    /**
     * @return \PHPUnit\Framework\MockObject\MockObject
     */
    private function getProductMock()
    {
        return $this
            ->getMockBuilder(Product::class)
            ->setMethods(
                [
                    'getPrice',
                ]
            )
            ->getMock();
    }

    /**
     * @return \PHPUnit\Framework\MockObject\MockObject
     */
    private function getDiscountMock()
    {
        return $this
            ->getMockBuilder(Discount::class)
            ->setMethods(
                [
                    'getValue',
                    'isFixed'
                ]
            )
            ->getMock();
    }

    /**
     * @return CartService
     */
    private function getInstance()
    {
        return new CartService();
    }

    /**
     *
     */
    public function testAddProduct()
    {
        $cartMock = $this->getCartMock();
        $productMock = $this->getProductMock();
        $addedProductCart = $this->getCartMock();
        $addedProduct = $this->getProductMock();
        $productMock
            ->method('getPrice')
            ->willReturn(10);

        $addedProduct
            ->method('getPrice')
            ->willReturn(20);


        $products = [$productMock];
        $cartMock
            ->method('getProducts')
            ->willReturn($products);

        array_push($products, $addedProduct);

        $addedProductCart
            ->method('getProducts')
            ->willReturn($products);

        $cartMock
            ->expects($this->once())
            ->method('addProduct')
            ->willReturn($addedProductCart);

        $instance = $this->getInstance();

        $instance->addProduct($cartMock, $productMock);
    }

    /**
     *
     */
    public function testRemoveProduct()
    {
        $cartMock = $this->getCartMock();
        $productMock = $this->getProductMock();
        $removedProductCartMock = $this->getCartMock();
        $productMock
            ->method('getPrice')
            ->willReturn(10);

        $products = [$productMock];
        $cartMock
            ->method('getProducts')
            ->willReturn($products);

        $removedProductCartMock
            ->method('getProducts')
            ->willReturn([$productMock]);

        $cartMock
            ->expects($this->once())
            ->method('removeProduct')
            ->willReturn($removedProductCartMock);

        $instance = $this->getInstance();

        $instance->removeProduct($cartMock, $productMock);
    }

    /**
     * @throws DiscountAlreadyExists
     */
    public function testAddDiscountWithDiscountAlreadyExistException()
    {
        $cartMock     = $this->getCartMock();

        $discountMock = $this->getDiscountMock();

        $cartMock
            ->expects($this->once())
            ->method('getDiscount')
            ->willReturn($discountMock);

        $instance = $this->getInstance();

        $this->expectException(DiscountAlreadyExists::class);

        $instance->addDiscount($cartMock, $discountMock);
    }

    /**
     * @throws DiscountAlreadyExists
     */
    public function testAddDiscountWithDiscountMoreException()
    {
        $cartMock     = $this->getCartMock();
        $discountMock = $this->getDiscountMock();
        $productMock = $this->getProductMock();

        $productPrice = 150;
        $discountPrice = 200;

        $productMock
            ->method('getPrice')
            ->willReturn($productPrice);


        $cartMock
            ->method('getDiscount')
            ->willReturn(null);

        $cartMock
            ->method('getProducts')
            ->willReturn([$productMock]);

        $cartMock
            ->expects($this->never())
            ->method('addDiscount');

        $discountMock
            ->method('getValue')
            ->willReturn($discountPrice);

        $discountMock
            ->method('isFixed')
            ->willReturn(true);

        $this->expectException(\Exception::class);

        $instance = $this->getInstance();

        $instance->addDiscount($cartMock, $discountMock);
    }

    /**
     * @throws DiscountAlreadyExists
     */
    public function testAddDiscount()
    {
        $cartMock     = $this->getCartMock();
        $cartMockWithAddedDiscount = $this->getCartMock();
        $discountMock = $this->getDiscountMock();
        $productMock = $this->getProductMock();

        $discountMock
            ->method('getValue')
            ->willReturn(100);

        $discountMock
            ->method('isFixed')
            ->willReturn(true);

        $productMock
            ->method('getPrice')
            ->willReturn(150);

        $cartMockWithAddedDiscount
            ->method('getDiscount')
            ->willReturn($discountMock);

        $cartMockWithAddedDiscount
            ->method('getProducts')
            ->willReturn([$productMock]);


        $cartMock
            ->method('getDiscount')
            ->willReturn(null);

        $cartMock
            ->method('getProducts')
            ->willReturn([$productMock]);

        $cartMock
            ->expects($this->once())
            ->method('addDiscount')
            ->willReturn($cartMockWithAddedDiscount);

        $instance = $this->getInstance();

        $instance->addDiscount($cartMock, $discountMock);
    }


    /**
     *
     */
    public function testRemoveDiscountWithDiscountDoesNotExistException()
    {
        $cartMock     = $this->getCartMock();

        $discountMock = $this->getDiscountMock();

        $cartMock
            ->expects($this->once())
            ->method('getDiscount')
            ->willReturn(null);

        $instance = $this->getInstance();

        $this->expectException(DiscountDoesNotExistOnTheCart::class);

        $instance->removeDiscount($cartMock, $discountMock);
    }

    /**
     * @throws DiscountDoesNotExistOnTheCart
     */
    public function testRemoveDiscount()
    {
        $cartMock     = $this->getCartMock();
        $cartMockWithRemovedDiscount = $this->getCartMock();
        $discountMock = $this->getDiscountMock();
        $productMock = $this->getProductMock();

        $discountMock
            ->method('getValue')
            ->willReturn(100);

        $discountMock
            ->method('isFixed')
            ->willReturn(false);

        $productMock
            ->method('getPrice')
            ->willReturn(150);

        $cartMockWithRemovedDiscount
            ->method('getDiscount')
            ->willReturn($discountMock);

        $cartMockWithRemovedDiscount
            ->method('getProducts')
            ->willReturn([$productMock]);


        $cartMock
            ->method('getDiscount')
            ->willReturn($discountMock);

        $cartMock
            ->method('getProducts')
            ->willReturn([$productMock]);

        $cartMock
            ->expects($this->once())
            ->method('removeDiscount')
            ->willReturn($cartMockWithRemovedDiscount);

        $instance = $this->getInstance();

        $instance->removeDiscount($cartMock, $discountMock);
    }


}