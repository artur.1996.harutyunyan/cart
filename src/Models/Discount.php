<?php


namespace TravelShared\Models;


interface Discount
{
    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @return boolean
     */
    public function isFixed(): bool;
}