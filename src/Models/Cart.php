<?php


namespace TravelShared\Models;


interface Cart
{
    /**
     * @return iterable
     */
    public function getProducts(): iterable;

    /**
     * @return Discount|null
     */
    public function getDiscount(): ?Discount;

    /**
     * @param Product $product
     * @return mixed
     */
    public function addProduct(Product $product): Cart;

    /**
     * @param Product $product
     * @param int $fixedDiscountedPrice
     * @return mixed
     */
    public function updateProductDiscount(Product $product, int $fixedDiscountedPrice): Cart;

    /**
     * @param Product $product
     * @return mixed
     */
    public function removeProduct(Product $product): Cart;

    /**
     * @param Discount $coupon
     * @return mixed
     */
    public function addDiscount(Discount $coupon): Cart;

    /**
     * @param Discount $coupon
     * @return mixed
     */
    public function removeDiscount(Discount $coupon): Cart ;

    /**
     * @param Product $product
     * @return mixed
     */
    public function incrementProductQuantity(Product $product): Cart;

}