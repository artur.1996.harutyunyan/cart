<?php


namespace TravelShared\Models;


interface Product
{
    /**
     * @return mixed
     */
    public function getPrice();
}