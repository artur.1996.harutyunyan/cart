<?php


namespace TravelShared\Exceptions;


class DiscountAlreadyExists extends \Exception
{
    /**
     * @var int
     */
    protected $code = 400;
    /**
     * @var string
     */
    protected $message = 'Only one discount can be applied to one cart.';
}