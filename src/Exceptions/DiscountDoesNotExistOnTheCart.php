<?php


namespace TravelShared\Exceptions;


class DiscountDoesNotExistOnTheCart extends  \Exception
{
    /**
     * @var int
     */
    protected $code = 400;

    /**
     * @var string
     */
    protected $message = 'The coupon does not exist on this cart.';
}