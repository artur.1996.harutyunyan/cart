<?php

namespace TravelShared\Services;

use TravelShared\Exceptions\DiscountAlreadyExists;
use TravelShared\Exceptions\DiscountDoesNotExistOnTheCart;
use TravelShared\Models\Cart;
use TravelShared\Models\Discount;
use TravelShared\Models\Product;

class CartService
{
    /**
     * @param Cart $cart
     * @param Product $product
     */
    public function addProduct(Cart $cart, Product $product)
    {
        $cart = $cart->addProduct($product);

        $this->cartUpdated($cart);
    }

    /**
     * @param Cart $cart
     */
    private function cartUpdated(Cart $cart)
    {
        foreach ($cart->getProducts() as $product) {
            /**
             * @var Product $product
             */
            $cart->updateProductDiscount($product, $this->getDiscountedPriceOfProduct($cart, $product));
        }
    }

    /**
     * @param Cart $cart
     * @param Product $product
     * @return float|int|mixed
     */
    private function getDiscountedPriceOfProduct(Cart $cart, Product $product)
    {
        $discount        = $cart->getDiscount();
        $productPrice    = $product->getPrice();
        $discountedPrice = $productPrice;

        if ($discount) {
            if ($discount->isFixed()) {
                $priceOfProducts = $this->getPriceOfProducts($cart);
                $discountedPrice = round(
                    ($productPrice / $priceOfProducts) * $productPrice,
                    2
                );

            } else {
                $discountedPrice = round( ($discount->getValue() / 100) * $productPrice);
            }
        }

        return $productPrice - $discountedPrice;
    }


    /**
     * @param Cart $cart
     * @return int|mixed
     */
    private function getPriceOfProducts(Cart $cart)
    {
        $price = 0;

        foreach ($cart->getProducts() as $product) {
            /**
             * @var Product $product
             */
            $price += $product->getPrice();
        }

        return $price;
    }


    /**
     * @param Cart $cart
     * @param Product $product
     */
    public function removeProduct(Cart $cart, Product $product)
    {
        $cart = $cart->removeProduct($product);

        $this->cartUpdated($cart);
    }

    /**
     * @param Cart $cart
     * @param Discount $discount
     * @throws DiscountAlreadyExists
     */
    public function addDiscount(Cart $cart, Discount $discount)
    {

        if ($cart->getDiscount()) {
            throw new DiscountAlreadyExists();
        }

        if (
            $discount->isFixed() &&
            $discount->getValue() > $this->getPriceOfProducts($cart)
        ) {
            throw new \Exception('The discount more products prices.');
        }

        $cart = $cart->addDiscount($discount);

        $this->cartUpdated($cart);
    }

    /**
     * @param Cart $cart
     * @param Discount $discount
     * @throws DiscountDoesNotExistOnTheCart
     */
    public function removeDiscount(Cart $cart, Discount $discount)
    {
        if (!$cart->getDiscount()) {
            throw new DiscountDoesNotExistOnTheCart();
        }

        $cart = $cart->removeDiscount($discount);

        $this->cartUpdated($cart);
    }

}